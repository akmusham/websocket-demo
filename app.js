var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

// io.on('connection', function(socket){
//   console.log('a user connected');
// });
io.on('connection', function(socket){
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);
    io.emit('chat message', msg);
  });
  socket.on('typing',function(whotyping) {
    console.log('whotyping',whotyping);
    socket.broadcast.emit('typing', whotyping)
  })
  socket.broadcast.emit('hi');
});

// io.emit('some event', { someProperty: 'some value', otherProperty: 'other value' });
// This will emit the event to all connected sockets


http.listen(3000, function(){
  console.log('listening on *:3000');
});
